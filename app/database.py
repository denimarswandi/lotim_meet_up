import pymysql

class Database:
  def __init__(self, app):
    self.app = app
  
  def getCon(self):
    con = self.app.config
    db = pymysql.connect(host="mysqldb", user=con['USER'], password=con['PWD'], database=con['DB'])
    return db
  
  def commitSQL(self, sql, params=[]):
    con = self.getCon()
    cur = con.cursor()
    try:
      cur.execute(sql, params)
      con.commit()
    except Exception as e:
      con.rollback()
      self.app.logger.info(e)
    finally:
      con.close()
  
  def getSingleQuery(self, sql, params=[]):
    con = self.getCon()
    cur = con.cursor(pymysql.cursors.DictCursor)
    try:
      cur.execute(sql, params)
      res = cur.fetchone()
      cur.close()
      return res
    except Exception as e:
      self.app.logger.info(e)
    finally:
      con.close()

  def getMultiQuery(self, sql, params=[]):
    con = self.getCon()
    cur = con.cursor(pymysql.cursors.DictCursor)
    try:
      cur.execute(sql, params)
      res = cur.fetchall()
      cur.close()
      return res
    except Exception as e:
      self.app.logger.info(e)
    finally:
      con.close()