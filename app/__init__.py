from flask import Flask
from flask_restful import Api
from app.database import Database
from flask_cors import CORS
from app.config import Config

app = Flask(__name__)
app.config.from_object(Config)
cors = CORS(app, resource={r"*":{"origins":"*"}})
api = Api(app)
db = Database(app)

from app import routes
