from flask_restful import Resource
from flask import request
from app.query.kamanriderQuery import KamanRaiderDB
from app import app

class KamanRaider(Resource):
  def post(self):
    data = request.get_json(force=True)
    KamanRaiderDB().save(data['name'], data['type'])
    return {'msg':'saved'}
  
  def get(self):
    return KamanRaiderDB().get()
   

    